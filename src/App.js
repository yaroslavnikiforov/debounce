import React, { Component } from "react";
import { debounce } from "lodash";
import "./App.css";

class App extends Component {
  state = {
    inputValue: "",
    isFieldsVisible: false
  };

  debounceEvent(...args) {
    this.debouncedEvent = debounce(...args);

    return e => {
      e.persist();
      return this.debouncedEvent(e);
    };
  }

  handleButtonClick = () =>
    this.setState(prevState => ({
      isFieldsVisible: !prevState.isFieldsVisible
    }));

  handleInputChange = e => this.setState({ inputValue: e.target.value });

  render() {
    const { inputValue, isFieldsVisible } = this.state;

    return (
      <div className="container">
        <input
          className="button"
          type="button"
          value="Show fields"
          onClick={this.handleButtonClick}
        />
        {isFieldsVisible ? (
          <div className="fields-wrapper">
            <input
              onChange={this.debounceEvent(this.handleInputChange, 1000)}
            />
            <textarea value={inputValue} readOnly />
          </div>
        ) : null}
      </div>
    );
  }

  componentWillUnmount() {
    this.debounceEvent.cancel();
  }
}

export default App;
